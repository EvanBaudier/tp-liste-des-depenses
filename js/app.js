// ---------------------------------------------------------------------------------------------------------------------
// Préparation de la couche modèle
// ---------------------------------------------------------------------------------------------------------------------

// On charge les données dans la couche "modèle"
const contactList = Object.create(ContactList);

function loadData() {
    for (let i = 0; i < 10; i++) {
        const contact = generateRandomContact();
        if (i < 3) {
            contact.isFavorite(true);
        }
        contactList.add(contact);
    }
}
loadData();

// ---------------------------------------------------------------------------------------------------------------------
// Affichage
// ---------------------------------------------------------------------------------------------------------------------

const $contacts = document.querySelector('.contact-list ul');


$contacts.addEventListener('click', function(event) {

    event.preventDefault();

    // console.log('On a cliqué précisément sur ', event.target);

    // Son parent (ou lui-même) est peut être un bouton de suppression
    if (event.target.closest('.contact-delete')) {

        const $contact = event.target.closest('.contact');
        const $phone = $contact.querySelector('.contact-phone')
        const phoneNumber = $phone.textContent.trim();

        // Ou en enchainant les appels :

        // const phoneNumber = event.target
        //     .closest('.contact')
        //     .querySelector('.contact-phone')
        //     .textContent;

        contactList.delete(phoneNumber);
        render();

    } else if (event.target.closest('.contact-edit')) {
        // TODO
        console.log('edit')
    } else if (event.target.closest('.contact-toggle-favorite')) {
        // TODO
        console.log('toggle favori')
    }

});

// on fait un rendu initial
render();

function render() {

    $contacts.innerHTML = ""

    // re rempli la liste HTML avec les données mises à jour
    for (let contact of contactList.getAll()) {
        createListItem(contact);
    }
}

/**
 * Affiche un contact dans la liste html
 */

let form = document.querySelector('#form-contact');


function createListItem(contact) {

    let icon = 'star';
    if (contact.isFavorite()) {
        icon = "star-fill";
    }


    const html = `
    <div class="contact">
        <div>
            <label class="contact-toggle-favorite">
                <svg class="bi" width="16" height="16" fill="currentColor">
                    <use xlink:href="img/icons/bootstrap-icons.svg#${icon}"/>
                </svg>
                <input type="checkbox">
            </label>
        </div>
        <div class="contact-infos">
            <h5 class="contact-fullname">${contact.getFirstName()} ${contact.getLastName()}</h5>
            <p class="contact-phone">
                ${contact.getPhone()}
            </p>
            <p>
                ${contact.getEmail()}
            </p>
        </div>
        <div class="contact-actions">
            <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-outline-secondary contact-delete">
                    <svg class="bi" width="16" height="16" fill="currentColor">
                        <use xlink:href="img/icons/bootstrap-icons.svg#trash"/>
                    </svg>
                </button>
                <button type="button" class="btn btn-outline-secondary contact-edit">
                    <svg class="bi" width="16" height="16" fill="currentColor">
                        <use xlink:href="img/icons/bootstrap-icons.svg#pen"/>
                    </svg>
                </button>
            </div>
        </div>
    </div>
`;

    const $li = document.createElement('li');
    $li.classList.add('list-group-item');
    $li.innerHTML = html;

    $contacts.appendChild($li);

}
