/**
 * "Plan de fabrication" d'un contact
 */
const Contact = {

    // propriétés
    //
    // on préfixe le nom par un underscore (_) pour
    // signifier aux développeurs qu'il ne faut pas les modifier
    // directement, mais plutôt passer par les getters/setters
    _firstName: "",
    _lastName: "",
    _phone: "",
    _email: "",
    _favorite: false,

    // méthodes
    getFirstName() {
        return ucFirst(this._firstName);
    },

    setFirstName(first) {
        if (typeof first === 'string') {
            this._firstName = first;
        }
    },

    getLastName() {
        return ucFirst(this._lastName);
    },

    setLastName(last) {
        if (typeof last === 'string') {
            this._lastName = last;
        }
    },

    getPhone() {
        return this._phone;
    },

    setPhone(phone) {

        if (typeof phone === "string") {
            // nettoyage
            phone = phone.trim();
            // validation
            if (isPhoneNumberValid(phone)) {
                this._phone = phone;
            }
        }

    },

    getEmail() {
        return this._email;
    },

    setEmail(email) {

        if (typeof email === "string") {
            email = email.trim();

            if (isEmailValid(email)) {
                this._email = email;
            }
        }

    },

    // getter et setter en même temps
    isFavorite(isFav) {

        // utilisation en "getter"
        if (isFav === undefined) {
            return this._favorite;
        }

        // si on donne un paramètre, ca veut dire qu'on définit
        // de l'info (donc un "setter")
        this._favorite = isFav;
    },


    isValid() {
        if (
            !isEmpty(this._phone) &&
            !isEmpty(this._email) &&
            (
                !isEmpty(this._firstName) ||
                !isEmpty(this._lastName)
            )
        ) {
            return true;
        }
        return false;
    },
}





