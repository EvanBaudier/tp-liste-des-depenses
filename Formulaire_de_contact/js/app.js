const contactList = Object.create(ContactList);
const $contacts = document.querySelector('.contact-list')
for (let i = 0; i < 1000; i++) {
    const contact = generateRandomContact();
    contactList.add(contact);


}

render();

function render() {
    let content = "";
    for (let contact of contactList.getAll()) {
      content  += createListItem(contact)
    }
    $contacts.innerHTML = content;
    const $deleteButtons = $contacts.querySelectorAll('.contact-delete');
}

createListItem()
function createListItem(contact) {
    return `
            <li class="list-group-item">
                <div class="contact">

                    <div>
                        <label class="contact-fav-toggle">
                            <svg class="bi" width="16" height="16" fill="currentColor">
                                <use xlink:href="img/icons/bootstrap-icons.svg#star"/>
                            </svg>
                            <input type="checkbox">
                        </label>
                    </div>
                    <div class="contact-infos">
                        <h5 class="contact-fullname">${contact.getFirstName()} ${contact.getLastName()}</h5>
                        <p class="contact-phone">
                            ${contact.getPhone()}
                        </p>
                        <p>
                            ${contact.getEmail()}
                        </p>
                    </div>
                    <div class="contact-actions">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-outline-secondary contact-delete">
                                <svg class="bi" width="16" height="16" fill="currentColor">
                                    <use xlink:href="img/icons/bootstrap-icons.svg#trash"/>
                                </svg>
                            </button>
                            <button type="button" class="btn btn-outline-secondary">
                                <svg class="bi" width="16" height="16" fill="currentColor">
                                    <use xlink:href="img/icons/bootstrap-icons.svg#pen"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </li>    
           `;
}
/*
const $li = document.createElement('li');
$li.className = "list-group-item";
$li.innerHTML = html;
$contacts.appendChild($li)

 */