const ContactList = {

    _data: [],

    /**
     * Retrieves all Contact within the list
     *
     * @return {Contact[]} an array of Contact objects
     */
    getAll() {
        return this._data;
    },

    /**
     * A the given contact to the list
     * @param {Contact} contact
     */
    add(contact) {

        // impossible d'ajouter un contact invalide
        if (!contact.isValid()) return;

        // impossible d'ajouter un contact déja existant
        if (this.exists(contact)) return;

        this._data.push(contact);
    },

    /**
     * Edit the given contact in the list
     * @param {Contact} data - the new contact data
     */
    edit(data) {
        if (!this.exists(data)) {
            // The object is not registered, so we add it
            // instead of editing it.
            this.add(data);
        } else {

            const contactPosition = this.getIndexOf(contact);
            const contact = this._data[contactPosition];

            contact.setFirstName(data.getFirstName())
            contact.setLastName(data.getLastName())
            contact.setPhone(data.getPhone())
            contact.setEmail(data.getEmail());

        }

    },

    /**
     * Removes the given contact from the list
     * @param {Contact} contact
     */
    delete(contact) {
        if (!this.exists(contact)) return;

        const contactPosition = this.getIndexOf(contact);
        this._data.splice(contactPosition, 1);
    },

    /**
     * Get the current number of Contact objects in the list
     * @returns number
     */
    count() {
        return this._data.length;
    },

    /**
     * Determines whether the list is empty or not
     * @return boolean - true if empty
     */
    isEmpty() {
        return this.count() === 0;
    },

    /**
     * Determines if the given contact is already registered in the list or not
     * @param {Contact} contact
     * @return boolean - true if the contact already exists
     */
    exists(contact) {
        for (let existingContact of this._data) {
            if (existingContact.getPhone() === contact.getPhone()) {
                return true;
            }
        }
        return false;
    },

    getIndexOf(contact) {
        for (let i = 0; i < this._data.length; i++) {
            const existingContact = this._data[i];

            if (existingContact.getPhone() === contact.getPhone()) {
                return i;
            }
        }
        return -1;
    },


    // A voir pour la suite ...
    // search(value) {},
    // filter(value) {},
    // getOne() {}


    debug() {
        console.log(JSON.parse(JSON.stringify(this._list)));
    }

}
